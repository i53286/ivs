#include <gtest/gtest.h>
#include <gmock/gmock-matchers.h>
#include "../math_lib/hdr/math_lib.h"
#include <limits>

using namespace testing;
using namespace std;

// SCITANI
TEST(add, no_throw) {
    int a = 1, b = 5;
    EXPECT_NO_THROW(Math_lib::Add(a, b));
}
TEST(add, int) {
    int a = 5, b = 50;
    EXPECT_EQ(Math_lib::Add(a, b), 55);
}
TEST(add, double) {
    double a = 2.56892982465, b = 8829435634.5546;
    EXPECT_DOUBLE_EQ(Math_lib::Add(a, b), 8829435637.12353);
}
TEST(add, out_of_double) {
    double a = numeric_limits<double>::max(), b = numeric_limits<double>::max();
    EXPECT_ANY_THROW(Math_lib::Add(a, b));
}
TEST (add, negative) {
    double a= -5.255, b=-3.247;
    EXPECT_DOUBLE_EQ (Math_lib::Add(a,b), -8.502);
}
TEST (add, first_negative) {
    double a= -2.222, b=2.223;
    EXPECT_DOUBLE_EQ (Math_lib::Add(a,b), 0.00099999999999988987);
}
TEST (add, second_negative) {
    double a=2.225, b=-3.0;
    EXPECT_DOUBLE_EQ (Math_lib::Add(a,b), -0.775);
}

// ODECITANI
TEST(sub, no_throw) {
    int a = 1, b = 5;
    EXPECT_NO_THROW(Math_lib::Sub(a, b));
}
TEST(sub, int) {
    int a = 55, b = 50;
    EXPECT_EQ(Math_lib::Sub(a, b), 5);
}
TEST(sub, double) {
    double a = 8829435634.5546, b = 2.56892982465;
    EXPECT_DOUBLE_EQ(Math_lib::Sub(a, b), 8829435631.98567);
}
TEST(sub, out_of_double) {
    double a = -numeric_limits<double>::max(), b = numeric_limits<double>::max();
    EXPECT_ANY_THROW(Math_lib::Sub(a, b));
}
TEST (sub, negative) {
    double a=-7.657, b=-3.001;
    EXPECT_DOUBLE_EQ(Math_lib::Sub(a,b), -4.656);
}
TEST (sub, first_negative) {
    double a=-5.250, b=3.249;
    EXPECT_DOUBLE_EQ(Math_lib::Sub(a,b), -8.499);
}
TEST (sub, second_negative) {
    double a=3.003, b=-5.005;
    EXPECT_DOUBLE_EQ(Math_lib::Sub(a,b), 8.008);
}

// NASOBENI
TEST(mul, no_throw) {
    int a = 5, b = 5;
    EXPECT_NO_THROW(Math_lib::Mul(a, b));
}
TEST(mul, int) {
    int a = 5, b = 5;
    EXPECT_EQ(Math_lib::Mul(a, b), 25);
}
TEST(mul, double) {
    double a = 5248.45624, b = 6287249.562;
    EXPECT_DOUBLE_EQ(Math_lib::Mul(a, b), 32998354196.11617);
}
TEST(mul, out_of_double) {
    double a = sizeof(double), b = numeric_limits<double>::max();
    EXPECT_ANY_THROW(Math_lib::Mul(a, b));
}
TEST (mul, negative) {
    double a=-5.0, b=-3.657;
    EXPECT_DOUBLE_EQ(Math_lib::Mul(a,b), 18.285);
}
TEST (mul, first_negative) {
    double a=-2.987, b=3.654;
    EXPECT_DOUBLE_EQ(Math_lib::Mul(a,b), -10.914498);
}
TEST (mul, second_negative) {
    double a=2.987, b=-3.654;
    EXPECT_DOUBLE_EQ(Math_lib::Mul(a,b), -10.914498);
}

// DELENI
TEST(div, no_throw) {
    int a = 5, b = 5;
    EXPECT_NO_THROW(Math_lib::Div(a, b));
}
TEST(div, int) {
    int a = 5, b = 5;
    EXPECT_EQ(Math_lib::Div(a, b), 1);
}
TEST(div, double) {
    double a = 6287249.562, b = 5248.45624;
    EXPECT_DOUBLE_EQ(Math_lib::Div(a, b), 1197.923594005235);
}
TEST(div, full_range) {
    double a = numeric_limits<double>::max(), b = numeric_limits<double>::max();
    EXPECT_DOUBLE_EQ(Math_lib::Div(a, b), 1);
}
TEST(div, divide_by_zero) {
    double a = 1, b = 0;
    EXPECT_ANY_THROW(Math_lib::Div(a, b));
}
TEST (div, negative) {
    double a=-5.321, b=-6.456;
     EXPECT_DOUBLE_EQ(Math_lib::Div(a,b), 0.82419454770755873);
}
TEST (div, first_negative) {
    double a=-5.321, b=6.456;
     EXPECT_DOUBLE_EQ(Math_lib::Div(a,b), -0.82419454770755873);
}
TEST (div, second_negative) {
    double a=5.321, b=-6.456;
     EXPECT_DOUBLE_EQ(Math_lib::Div(a,b), -0.82419454770755873);
}

// FACTORIAL

TEST(factorial, no_throw) {
    int a = 12;
    EXPECT_NO_THROW (Math_lib::Factorial(a));
}
TEST(factorial, int) {
    int a=11;
    EXPECT_EQ(Math_lib::Factorial(a), 39916800);
}
TEST (factorial, negative_int) {
    int a = -5;
    EXPECT_ANY_THROW(Math_lib::Factorial(a));
}
TEST (factorial, out_of_long_long) {
    int a = numeric_limits<long long>::max();
    EXPECT_ANY_THROW(Math_lib::Factorial(a));
}
TEST (factorial, zero) {
    int a =0;
    EXPECT_EQ (Math_lib::Factorial(a),1);
}

//MOCNINA
TEST (power, no_throw) {
    double a=5.5, b=6.7;
    EXPECT_NO_THROW(Math_lib::Power(a, b));
}
TEST (power, int) {
    int a=78, b=5;
    EXPECT_EQ (Math_lib::Power(a, b), 2887174368);
}
TEST (power, double) {
    double a=2.33, b=6.56;
    EXPECT_DOUBLE_EQ (Math_lib::Power(a, b), 256.95368003107438);
}
TEST (power, zero) {
    double a=0, b=5.65;
    EXPECT_EQ (Math_lib::Power (a, b), 0);
}
TEST (power, zero2) {
    double a=-5.650, b=0;
    EXPECT_EQ (Math_lib::Power (a, b), 1);
}
TEST (power, out_of_double) {
    double a=numeric_limits<double>::max(), b=numeric_limits<double>::max();
    EXPECT_ANY_THROW(Math_lib::Power(a,b));
}
TEST (power, negative) {
    double a=-0.1, b=-4.5;
    EXPECT_ANY_THROW(Math_lib::Power(a,b));
}
TEST (power, first_negative) {
    double a=-0.1, b=4.5;
    EXPECT_ANY_THROW(Math_lib::Power(a,b));
}
TEST (power, second_negative) {
    double a=0.1, b=-4.5;
    EXPECT_DOUBLE_EQ(Math_lib::Power(a,b), 31622.776601683785);
}

//ODMOCNINA
TEST (root, no_throw) {
    double a=2.0, b=4.0;
    EXPECT_NO_THROW(Math_lib::Root(a,b));
}
TEST (root, int) {
    int a=5, b=8;
    EXPECT_DOUBLE_EQ(Math_lib::Root(a,b), 1.2228445449938519);
}
TEST (root, double) {
    double a=2.5, b=6.5;
    EXPECT_DOUBLE_EQ(Math_lib::Root(a,b), 1.1513875783540721);
}
TEST (root, zero) {
    double a=0.0, b=5.256;
    EXPECT_ANY_THROW(Math_lib::Root(a,b));
}
TEST (root, zero2) {
    double a=2.25, b=0.0;
    EXPECT_EQ(Math_lib::Root(a,b),0);
}
TEST (root, negative) {
    double a=-6.0, b=-4.45;
    EXPECT_ANY_THROW(Math_lib::Root(a,b));
}
TEST (root, first_negative) {
    double a=-128, b=4;
    EXPECT_ANY_THROW(Math_lib::Root(a,b));
}
TEST (root, second_negative) {
    double a=8.0, b=-4.45;
    EXPECT_ANY_THROW(Math_lib::Root(b, a));
}
TEST (root, second_negative_odd) {
    int a=3, b=-27;
    EXPECT_EQ(Math_lib::Root(b,a), -3);
}

//PROCENTO
TEST (percent, no_throw) {
    int a=100, b=25;
    EXPECT_NO_THROW(Math_lib::Percent(a,b));
}
TEST (percent, int) {
    int a=200, b=1;
    EXPECT_EQ(Math_lib::Percent(a,b), 2);
}
TEST (percent, double) {
    double a=303.257, b=256.2;
    EXPECT_DOUBLE_EQ(Math_lib::Percent(a,b), 776.944434);
}
TEST (percent, zero) {
    int a=0, b=0;
    EXPECT_EQ(Math_lib::Percent(a,b),0);
}
TEST (percent, zero2) {
    double a=2.456, b=0.0;
    EXPECT_DOUBLE_EQ(Math_lib::Percent(a,b), 0);
}
TEST (percent, zero3) {
    double a=0.0, b=65.564;
    EXPECT_DOUBLE_EQ(Math_lib::Percent(a,b), 0.0);
}
TEST (percent, out_of_double) {
    double a=numeric_limits<double>::max(), b=numeric_limits<double>::max();
    EXPECT_ANY_THROW(Math_lib::Percent(a,b));
}
TEST (percent, negative) {
    double a=-10.0, b=-25.3;
    EXPECT_ANY_THROW(Math_lib::Percent(a,b));
}
TEST (percent, first_negative) {
    double  a=-10.564, b=5.456;
    EXPECT_ANY_THROW(Math_lib::Percent(a,b)); //-0.57637184
}
TEST (percent, second_negative) {
    double a=101.101, b=-25.3;
    EXPECT_DOUBLE_EQ(Math_lib::Percent(a,b), 75.522447);
}


include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG += thread
CONFIG += c++11
CONFIG -= qt

HEADERS +=     calc_lib_tests.h

SOURCES +=     calc_lib_tests.cpp


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../math_lib/lib/release/ -lmath
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../math_lib/lib/debug/ -lmath
else:unix: LIBS += -L$$PWD/../math_lib/lib/ -lmath

INCLUDEPATH += $$PWD/../math_lib/lib/debug
DEPENDPATH += $$PWD/../math_lib/lib/debug

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/libmath.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/libmath.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/math.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/math.lib
else:unix: PRE_TARGETDEPS += $$PWD/../math_lib/lib/libmath.a


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../math_lib/lib/release/ -lmath
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../math_lib/lib/debug/ -lmath
else:unix: LIBS += -L$$PWD/../math_lib/lib/ -lmath

INCLUDEPATH += $$PWD/../math_lib/lib/release
DEPENDPATH += $$PWD/../math_lib/lib/release

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/libmath.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/libmath.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/math.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/math.lib
else:unix: PRE_TARGETDEPS += $$PWD/../math_lib/lib/libmath.a

/**
*@file math_lib.h
*@author Michal Reznak
*@date 13.04.2017
*@brief Hlavickovy soubor matematicke knihovny
*/

#ifndef MATH_LIB_H
#define MATH_LIB_H
/**
*@class Math_lib
*@brief Deklarace funkci pro matematickou knihovnu
*/

class Math_lib
{

public:
    /**
    *@brief Funkce pro scitani
    *@return double
    */
    static double Add(double a, double b);
    /**
    *@brief Funkce pro odecitani
    *@return double
    */
    static double Sub(double a, double b);
    /**
    *@brief Funkce pro nasobeni
    *@return double
    */
    static double Mul(double a, double b);
    /**
    *@brief Funkce pro deleni
    *@return double
    */
    static double Div(double a, double b);
    /**
    *@brief Funkce pro vypocet faktorialu
    *@return long long
    */
    static long long Factorial(int a);
    /**
    *@brief Funkce pro vypocet mocniny
    *@return double
    */
    static double Power(double base, double value);
    /**
    *@brief Funkce pro vypocet faktorialu
    *@return double
    */
    static double Root(double base, double value);
    /**
    *@brief Funkce pro vypocet zbytku po deleni
    *@return double
    */
    static double Percent(double base, double value);
    /** End of class Math_lib */
};

#endif // MATH_LIB_H

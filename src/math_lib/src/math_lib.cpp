/**
*@file math_lib.h
*@date 13.04.2017
*@brief Matematicka knihovna
*@author Michal Reznak
*/
#include "../hdr/math_lib.h"
#include <limits>
#include <math.h>
#include <stdexcept>
#include <cfenv>

using namespace std;
/**
*@brief Funkce pro scitani dvou cisel, v pripade spatneho vstupu vrati vyjimku
*@param a Prvni scitanec
*@param b Druhy scitanec
*@return double
*/

double Math_lib::Add(double a, double b) {

    //nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);

    // vypocet vysledne hodnoty.
    /**
     Vypocet vysledne hodnoty
     @code
     double result = a + b;
     @endcode
    */
    double result = a + b;

    // vyjimka pri chybe
        /**
     V pripade preteceni vytvori vyjimku.
     @code
     if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Cisla prilis velka");
        }
     @endcode
    */
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Cisla prilis velka");
    }

    return result;
}
/**
*@brief Funkce pro odcitani dvou cisel
* V pripade spatneho vstupu vrati vyjimku
*@param a Prvni cislo
*@param b Druhe cislo
*@return double
*/
double Math_lib::Sub(double a, double b) {

    // nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);

    // vypocet vysledne hodnoty.
        /**
     Vypocet vysledne hodnoty
     @code
     double result = a - b;
     @endcode
    */
    double result = a - b;

    // vyjimka pri chybe
    /**
     V pripade preteceni vytvori vyjimku.
     @code
     if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Chyba pri odcitani");
    }
     @endcode
    */
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Chyba pri odcitani");
    }

    return result;
}
/**
*@brief Funkce pro nasobeni dvou cisel
* V pripade spatneho vstupu vrati vyjimku
*@param a Prvni cislo
*@param b Druhy cislo
*@return double
*/

double Math_lib::Mul(double a, double b) {

    // nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);

    // vypocet vysledne hodnoty.
     /**
     Vypocet vysledne hodnoty.
     @code
     double result = a * b;
     @endcode
    */
    double result = a * b;

    // vyjimka pri chybe
     /**
     V pripade preteceni vytvori vyjimku.
     @code
     if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Cisla prilis velka");
    }

     @endcode
    */
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Cisla prilis velka");
    }

    return result;
}

/**
*@brief Funkce pro deleni cisla
* V pripade spatneho vstupu vrati vyjimku
*@param a delenec
*@param b delitel
*@return double
*/

double Math_lib::Div(double a, double b) {

    // nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);

    // vypocet vysledne hodnoty.
    /**
     Vypocet vysledne hodnoty.
     @code
     double result = a / b;
     @endcode
    */
    double result = a / b;

    // vyjimka pri chybe
     /**
     Vytvori vyjimku v pripade preteceni nebo deleni 0.
     @code
     if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW | FE_DIVBYZERO) != 0) {
        throw new invalid_argument("Chyba pri deleni");
     }
     @endcode
    */
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW | FE_DIVBYZERO) != 0) {
        throw new invalid_argument("Chyba pri deleni");
    }

    return result;
}
/**
*@brief Funkce pro vypocet faktorialu
* V pripade zaporneho cisla na vstupu vrati vyjimku
*@param a cislo
*@return long long
*/
long long Math_lib::Factorial(int a) {

    // nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);

    long long result = 1;

    // faktorial z nuly je nula
    if (a == 0) {
        return 1;
    }

    // pouze nezaporne cisla
 /**
     V pripade zaporneho vstupu vytvori vyjimku.
     @code
         if (a < 0) {
        throw new runtime_error("Pouze nezaporne cisla");
    }
    @endcode
    */
    if (a < 0) {
        throw new runtime_error("Pouze nezaporne cisla");
    }

    // vypocet faktorialu
    /**
     Vypocet faktorialu.
     @code
    for (long long i = a; i > 0; --i) {
        result *= i;
    }
    @endcode
    */

    for (long long i = a; i > 0; --i) {
        result *= i;
    }

    // vyjimka pri chybe
/**
 V pripade preteceni vytvori vyjimku.
 @code
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Cislo prilis velke");
    }
 @endcode
*/
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW) != 0) {
        throw new invalid_argument("Cislo prilis velke");
    }

    return result;
}
/**
*@brief Funkce pro mocninu cisla
*Pokud je mocnec a mocnitel mensi nez 0 nebo pokud je mocnenec mensi nez 0 a zaroven mocnitel >= 0 vrati vyjimku
*@param base Mocnenec
*@param value Mocnitel
*@return double
*/
double Math_lib::Power(double base, double value) {

    // nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);

    double result;

    // ^0 je vzdy 1
    if (value == 0) {
        return 1;
    }

    // nelze ze zaporneho cisla

 /**
     Vytvori vyjimku v pripade preteceni nebo neplatneho vstupu.
     @code
    if (base < 0 && value < 0) {
        throw new invalid_argument("Neplatny vstup");
    }
    else if (base < 0 && value >= 0) {
        throw new invalid_argument("Neplatny vstup");
    }


    if (fetestexcept(FE_OVERFLOW | FE_UNDERFLOW) != 0) {
        throw new invalid_argument("Prilis velka cisla");
    }
     @endcode
    */
    if (base < 0 && value < 0) {
        throw new invalid_argument("Neplatny vstup");
    }
    else if (base < 0 && value >= 0) {
        throw new invalid_argument("Neplatny vstup");
    }

    result = pow(base, value);

    if (fetestexcept(FE_OVERFLOW | FE_UNDERFLOW) != 0) {
        throw new invalid_argument("Prilis velka cisla");
    }

    return result;
}

/**
*@brief Funkce pro odmocninu
* V pripade spatneho vstupu vrati vyjimku
*@param base cislo
*@param value odmocnina
*@return double
*/

double Math_lib::Root(double base, double value) {

    // nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);

    double result;
     /**
     Vytvoreni vyjimky v pripade nulte odmocniny a sude odmocniny ze zaporneho cisla.
     @code
         if (base == 0) {
        throw new invalid_argument("Nelze delit nulou");
    }

    else if (base < 0 && (long long)value % 2 == 0) {
        throw new invalid_argument("Nelze suda odmocnina ze zaporneho cisla");
    }
     @endcode
    */
    if (base == 0) {
        throw new invalid_argument("Nelze delit nulou");
    }

    else if (value == 0) {
        return 0;
    }

    else if (base < 0 && (long long)value % 2 == 0) {
        throw new invalid_argument("Nelze suda odmocnina ze zaporneho cisla");
    }

    result = pow(base, 1.0/value);
/**
Kontrola lichosti odmocniny v pripade zaporneho cisla.
 @code
    if (base < 0 && ((long long)value % 2 == 1)) {
        base = -base;
        result = pow(base, 1.0/value);
        result = -result;
    }
 @endcode
*/
    if (base < 0 && ((long long)value % 2 == 1)) {
        base = -base;
        result = pow(base, 1.0/value);
        result = -result;
    }
/**
* Kontrola preteceni a deleni 0.
 @code
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW | FE_DIVBYZERO)) {
        throw new invalid_argument("Chyba pri vypoctu");
    }

 @endcode
*/
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW | FE_DIVBYZERO)) {
        throw new invalid_argument("Chyba pri vypoctu");
    }

    return result;
}

/**
*@brief Funkce pro vypocet procenta
*Vraci zadane procento z cisla.
*@param base procento
*@param value cislo
*@return double
*/

double Math_lib::Percent(double base, double value) {

    // nulovani priznaku
    /**
     Nulovani priznaku
     @code
     feclearexcept(FE_ALL_EXCEPT);
     @endcode
    */
    feclearexcept(FE_ALL_EXCEPT);
/**
 Vytvoreni vyjimky pro zaporne procento
 @code
    if (base < 0) {
        throw new invalid_argument("Nelze zaporne procenta");
    }
 @endcode
*/
    if (base < 0) {
        throw new invalid_argument("Nelze zaporne procenta");
    }

    double result;
/**
 V pripade zaporneho procenta: obrati znamenko, vypocita pocet procent a odecte od hlavniho cisla
 @code
     if (value < 0) {
        value = -value;
        value /= 100;
        result = -(base * value);
        result = base + result;
    }
    else {
        value /= 100; // prevedeni z procent
        result = base * value;
    }

 @endcode
*/
    if (value < 0) {
        value = -value;
        value /= 100;
        result = -(base * value);
        result = base + result;
    }
    else {
        value /= 100; // prevedeni z procent
        result = base * value;
    }
/**
 V pripade preteceni vytvori vyjimku.
 @code
 if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW)) {
        throw new invalid_argument("Prilis velke cislo");
 }
 @endcode
*/
    if (fetestexcept(FE_UNDERFLOW | FE_OVERFLOW)) {
        throw new invalid_argument("Prilis velke cislo");
    }

    return result;
}

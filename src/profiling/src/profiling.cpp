#include <iostream>
#include "../math_lib/hdr/math_lib.h"

double odchylka (int n, char *pole[])
{
    double s=0;     
    double suma=0;
    double pomocne_x=0;
    double prumer_x=0;
    double cislo =0;
    double s_pomocna=0;
    double n2 =0;

    if ((n == 1) || (n<1)) {
        printf ("Nebyly zadany argumenty.\n");
        return 1.0;
    }

    n--;
    for (int i=1; i<(n+1); i++) {
        
        cislo = strtod(pole[i], NULL);
        
        if (cislo==NULL) {
            printf ("Chyba pri prevedeni cisel.\n");
            return 1.0;
        }

        pomocne_x = Math_lib::Power(cislo,2);
        suma = Math_lib::Add(suma, pomocne_x);    //suma = suma + pomocne_x;
        pomocne_x =0;

        prumer_x = Math_lib::Add(prumer_x, cislo);      //prumer_x = prumer_x + cislo;
        cislo =0;
    }

    prumer_x=Math_lib::Div(prumer_x, n);    //prumer_x = prumer_x / n;

    n2 = n;

    s = Math_lib::Sub(n2, 1);   //s=1/(n2-1);
    s= Math_lib::Div(1, s);


    s_pomocna =Math_lib::Power(prumer_x, 2);
    s_pomocna = Math_lib::Mul(n, s_pomocna);

    suma = Math_lib::Sub(suma, s_pomocna);  //s = s * (suma - s_pomocna);
    s = Math_lib::Mul(s, suma);

    s = Math_lib::Root (s, 2);

    printf ("Odchylka se rovna: %f \n", s);
    
    return s;
}

int main(int argc, char *argv[])
{
    odchylka (argc, argv);

    
    getchar();
    return 0;
}

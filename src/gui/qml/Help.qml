import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.0

ApplicationWindow {
    id: help
    visible: true
    width: 600; height: 600
    x: Screen.width - width - 50
    y: Screen.height - height - 50
    color: "#aaa"

    TextArea {
        id: textArea
        anchors.fill: parent
        readOnly: true
        font.family: "DejaVu"
        textFormat: Text.RichText
        backgroundVisible: false
        text: "
  <title>Napoveda_ke_kalkulacce_Frosty</title>
  </head>
  <body>
  <h1>Návod k použití</h1>
  <h2>Důležité informace</h2>
  <p>
  Pro správný výsledek je nutné nejprve zadat číslo a poté stisknout tlačítko s požadovanou operací.
  Pro získání výpočtu je možné stisknout tlačítko \"rovná se\" nebo opakovat stisknutí tlačítka s operací.
  Výsledek se zobrazí v levém horním roku dipleje. Je možné pokračovat další operací,
  v takovém případě se bere přechozí výsledek jako první operand.
  Je možné také kombinovat operace např. sčítání a odmocninu: po zadání čísla, stisknutí tlačítka \"plus\",
  zadání dalšího čísla a nasledném stisku \"odmocniny\" se čísla nejprve sečtou a poté se výsledek odmocní
  (postup je analogicky pužitelný i pro ostatní podporované operace).

  </p>
  <h3>Chyby</h3>
  <p>V případě zadání neplatné hodnoty např. odmocnina ze záporného čísla  se objeví na displeji znak NAND</p>
  <h3>Symboly</h3>
  <p>
   <em>RED/BLUE</em> -- Slouží k přepínání barevného pozadí kalkulačky.<br />
   <em>!</em> -- Slouží k výpočtu faktoriálu z čísla zadaného na spodním řádku displeje.<br />
   <em>%</em> -- Slouží pro procentový výpočet ze zadané hodnoty. Př. Pro výpočet 10% ze 100 je nutné nejprve zadat hodnotu 100 zmáčknout = a poté zadat hodnotu 10 a zmáčknout %.<br />
   <em>C</em> -- Slouží k vynulování kalkulačky.<br />
   <em>CE</em> -- Slouží pro vynulování spodního řádku.<br />
   <em>\u20EA</em> -- Slouží pro vymazání jednoho čísla ze spodního řádku.<br />
  </p>
  <h3>Dodatečné informace</h3>
  <p>Kalkulačka Frosty je šířena pod licencí LGPL \"https://www.gnu.org/licenses/lgpl-3.0.en.html\".</p>
"
    }
}

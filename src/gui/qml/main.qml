import QtQuick 2.6
import QtQuick.Window 2.2
import QtMultimedia 5.8
import Cal 1.0

Window {
    visible: true
    width: 350
    height: 550
    x: Screen.width*2/3 - width/2
    y: Screen.height/2 - height/2
    minimumHeight: 550
    minimumWidth: 350
    maximumHeight: 550
    maximumWidth: 350
    title: qsTr("Frosty")
    color: cal.theme ? "#331c1c" : "#14657c"
    Image {
        id: background
        anchors.fill: parent
        source: "../images/frosty_bg.png"
    }

    Cal {
        id: cal
        property string input_text: ""
        property string result_text: "It's cold outside."
        property double result_number: 0
        property string operand: "plus"
        property bool theme: false // false = blue, true = red

        function calculate(sign, oper) {
            if (oper == "root") {
                cal.operand = oper
                cal.result_number = cal.result(cal.result_number, cal.input_text, cal.operand) // res plus input --  save to result
                cal.result_text = sign + cal.result_number // print result and plus
                cal.input_text = "" // clear input
                cal.operand = ""
            }
            else if (cal.input_text != "" && cal.operand == "result") {
                console.log("RESET")
                cal.result_number = cal.input_text
                cal.result_text = cal.result_number + sign
                cal.input_text = ""
                cal.operand = oper
            }
            else {
                cal.result_number = cal.result(cal.result_number, cal.input_text, cal.operand) // res plus input --  save to result
                cal.result_text = cal.result_number + sign // print result and plus
                cal.input_text = "" // clear input
                cal.operand = oper
            }
        }
    }

    Rectangle {
        id: theme
        x: 20
        y: 20
        width: 65
        height: 25
        radius: 5
        color: area_theme.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        Text {
            text: cal.theme ? "BLUE" : "RED"
            fontSizeMode: Text.Fit
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: area_theme
            anchors.fill: parent
            onClicked: cal.theme ? cal.theme = false : cal.theme = true
        }
    }

//    Rectangle {
//        id: frosty
//        x: 110
//        y: 20
//        width: 65
//        height: 25
//        color: area_frosty.pressed ? "#00f" : "#fff"
//        Audio {
//            id: sound_frosty
//            source: "../sound/frosty.wav"
//        }
//        MouseArea {
//            id: area_frosty
//            anchors.fill: parent
//            onClicked: sound_frosty.status == 3 ? sound_frosty.play() : sound_frosty.pause()
//        }
//        Text {
//            text: "Frosty"
//            fontSizeMode: Text.Fit
//            font.pointSize: 30
//            anchors.fill: parent
//            verticalAlignment: Text.AlignVCenter
//            horizontalAlignment: Text.AlignHCenter
//        }
//    }

    Rectangle {
        id: help
        x: 145
        y: 20
        width: 120
        height: 25
        radius: 5
        color: area_help.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        Text {
            text: "Nápověda"
            anchors.fill: parent
            fontSizeMode: Text.VerticalFit
            font.pointSize: 30
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
        MouseArea {
            id: area_help
            anchors.fill: parent
            onClicked: {
                var component = Qt.createComponent("Help.qml")
                var window    = component.createObject(help)
                window.show()
            }
        }
    }

    Rectangle {
        id: display
        x: 20; y:60
        width: 310; height: 130
        radius: 3
        color: cal.theme ? "#af5f5f" : "#c0c0f0"
        Image {
            anchors.fill: parent
            source: "../images/frosty_display.png"
        }
        Text {
            id: textResult
            text: cal.result_text
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            font.pixelSize: 34
            anchors.bottomMargin: display.height/2
            anchors.fill: parent
            fontSizeMode: Text.Fit
            verticalAlignment: Text.AlignVCenter
        }
        Text {
            id: textInput
            text: cal.input_text
            style: Text.Normal
            anchors.rightMargin: 10
            anchors.leftMargin: 10
            horizontalAlignment: Text.AlignRight
            font.pixelSize: 34
            anchors.topMargin: display.height/2
            anchors.fill: parent
            fontSizeMode: Text.Fit
            verticalAlignment: Text.AlignVCenter
        }
        Item {
            id: keyHandler
            focus: true
            Keys.onPressed: {
                if (event.key == Qt.Key_0) {
                    cal.input_text += '0'
                }
                else if (event.key == Qt.Key_1) {
                    cal.input_text += '1'
                }
                else if (event.key == Qt.Key_2) {
                    cal.input_text += '2'
                }
                else if (event.key == Qt.Key_3) {
                    cal.input_text += '3'
                }
                else if (event.key == Qt.Key_4) {
                    cal.input_text += '4'
                }
                else if (event.key == Qt.Key_5) {
                    cal.input_text += '5'
                }
                else if (event.key == Qt.Key_6) {
                    cal.input_text += '6'
                }
                else if (event.key == Qt.Key_7) {
                    cal.input_text += '7'
                }
                else if (event.key == Qt.Key_8) {
                    cal.input_text += '8'
                }
                else if (event.key == Qt.Key_9) {
                    cal.input_text += '9'
                }
                else if (event.key == Qt.Key_Period) {
                    if (!cal.input_text.includes("."))
                        cal.input_text += '.'
                }
                else if (event.key == Qt.Key_Backspace) {
                    cal.input_text = cal.input_text.substring(0, cal.input_text.length -1)
                }

                // PLUS
                else if (event.key == Qt.Key_Plus) {
                    cal.calculate("+", "plus")
                }

                // MINUS
                else if (event.key == Qt.Key_Minus) {
                    cal.calculate("-", "minus")
                }

                // DIVIDE
                else if (event.key == Qt.Key_Slash) {
                    cal.calculate("/", "slash")
                }

                // MULTIPLY
                else if (event.key == Qt.Key_Asterisk) {
                    cal.calculate("*", "asterisk")
                }

                // RESULT
                else if (event.key == Qt.Key_Enter) {
                        cal.calculate("", "result")
                }
            }
        }
    }

    Rectangle {
        id: key_fac
        x: 20
        y: 205
        width: 50
        height: 50
        radius: 5
        color: area_fac.pressed ? "#fff" : "transparent"
        Image {
            id: pret_fac
            anchors.fill: parent
            source: "../images/frosty_button1.png"
        }
        Image {
            id: bg_fac
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_fac
            anchors.fill: parent
            onClicked: {
                if (cal.input_text.includes('.')) {
                    cal.calculate("", "result")
                    cal.input_text = ""
                    cal.result_text = "Invalid input"
                } else {
                    cal.calculate("", "factorial")
                    cal.calculate("", "result")
                }
            }
        }
        Text {
            anchors.fill: parent
            text: "!"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }


    Rectangle {
        id: key_pow
        x: 20
        y: 270
        width: 50
        height: 50
        radius: 5
        color: area_pow.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button3.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_pow
            anchors.fill: parent
            onClicked: {
                cal.calculate("", "power")
                cal.input_text = 2
                cal.calculate("", "result")
            }
        }
        Text {
            anchors.fill: parent
            text: "^2"
            style: Text.Normal
            fontSizeMode: Text.VerticalFit
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_pown
        x: 20
        y: 335
        width: 50
        height: 50
        radius: 5
        color: area_pown.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_pown
            anchors.fill: parent
            onClicked: {
                cal.calculate(" ^", "power")
            }
        }
        Text {
            anchors.fill: parent
            text: "^"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_root
        x: 20
        y: 400
        width: 50
        height: 50
        radius: 5
        color: area_root.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button3.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_root
            anchors.fill: parent
            onClicked: {
                cal.calculate("", cal.operand)
                cal.input_text = 2
                cal.calculate("", "root")
            }
        }
        Text {
            anchors.fill: parent
            text: "\u221A"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_rootn
        x: 20
        y: 465
        width: 50
        height: 50
        radius: 5
        color: area_rootn.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button1.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_rootn
            anchors.fill: parent
            onClicked: {
                cal.calculate(" ^\u221A", cal.operand)
                cal.calculate("", "rootn")
                cal.result_text = "^\u221A " + cal.result_text
            }
        }
        Text {
            anchors.fill: parent
            text: "^\u221A"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_per
        x: 85
        y: 205
        width: 50
        height: 50
        radius: 5
        color: area_per.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button3.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_per
            anchors.fill: parent
            onClicked: {
                if (cal.operand == "result") {
                    cal.operand = "percent"
                    cal.calculate("", "result")
                } else {
                    var tmp = cal.result_number
                    cal.operand = "percent"
                    cal.calculate("", "result")
                    cal.result_number += tmp
                    cal.result_text = cal.result_number
                }
            }
        }
        Text {
            anchors.fill: parent
            text: "%"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_div
        x: 85
        y: 270
        width: 50
        height: 50
        radius: 5
        color: area_div.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button1.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_div
            anchors.fill: parent
            onClicked: cal.calculate("/", "slash")
        }
        Text {
            anchors.fill: parent
            text: "÷"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_mul
        x: 85
        y: 335
        width: 50
        height: 50
        radius: 5
        color: area_mul.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button3.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_mul
            anchors.fill: parent
            onClicked: cal.calculate("*", "asterisk")
        }
        Text {
            anchors.fill: parent
            text: "X"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_sub
        x: 85
        y: 400
        width: 50
        height: 50
        radius: 5
        color: area_sub.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_sub
            anchors.fill: parent
            onClicked: cal.calculate("-", "minus")
        }
        Text {
            anchors.fill: parent
            text: "-"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_plus
        x: 85
        y: 465
        width: 50
        height: 50
        radius: 5
        color: area_plus.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button1.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_plus
            anchors.fill: parent
            onClicked: cal.calculate("+", "plus")
        }
        Text {
            anchors.fill: parent
            text: "+"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_erasa
        x: 150
        y: 205
        width: 50
        height: 50
        radius: 5
        color: area_pown.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button3.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_erasa
            anchors.fill: parent
            onClicked: {
                cal.calculate("", "result")
                cal.result_number = 0
                cal.result_text = ""
            }
        }
        Text {
            anchors.fill: parent
            text: "C"
            fontSizeMode: Text.VerticalFit
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }

    Rectangle {
        id: key_7
        x: 150
        y: 270
        width: 50
        height: 50
        radius: 5
        color: area_7.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_7
            anchors.fill: parent
            onClicked: cal.input_text += "7"
        }
        Text {
            text: "7"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_4
        x: 150
        y: 335
        width: 50
        height: 50
        radius: 5
        color: area_pown.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button1.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_4
            anchors.fill: parent
            onClicked: cal.input_text += "4"
        }
        Text {
            text: "4"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_1
        x: 150
        y: 400
        width: 50
        height: 50
        radius: 5
        color: area_1.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_1
            anchors.fill: parent
            onClicked: cal.input_text += "1"
        }
        Text {
            text: "1"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_dot
        x: 150
        y: 465
        width: 50
        height: 50
        radius: 5
        color: area_dot.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_dot
            anchors.fill: parent
            onClicked: {
                if (!cal.input_text.includes("."))
                    cal.input_text += '.'
            }
        }
        Text {
            text: "."
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_eras
        x: 215
        y: 205
        width: 50
        height: 50
        radius: 5
        color: area_eras.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_eras
            anchors.fill: parent
            onClicked: cal.input_text = ""
        }
        Text {
            text: "CE"
            fontSizeMode: Text.Fit
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_8
        x: 215
        y: 270
        width: 50
        height: 50
        radius: 5
        color: area_8.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_8
            anchors.fill: parent
            onClicked: cal.input_text += "8"
        }
        Text {
            text: "8"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_5
        x: 215
        y: 335
        width: 50
        height: 50
        radius: 5
        color: area_5.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_5
            anchors.fill: parent
            onClicked: cal.input_text += "5"
        }
        Text {
            text: "5"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_2
        x: 215
        y: 400
        width: 50
        height: 50
        radius: 5
        color: area_2.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_2
            anchors.fill: parent
            onClicked: cal.input_text += "2"
        }
        Text {
            text: "2"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_0
        x: 215
        y: 465
        width: 50
        height: 50
        radius: 5
        color: area_0.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_0
            anchors.fill: parent
            onClicked: cal.input_text += "0"
        }
        Text {
            text: "0"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_back
        x: 280
        y: 205
        width: 50
        height: 50
        radius: 5
        color: area_back.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_back
            anchors.fill: parent
            onClicked: cal.input_text = cal.input_text.substring(0, cal.input_text.length -1)
        }
        Text {
            text: "\u20EA"
            fontSizeMode: Text.VerticalFit
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_9
        x: 280
        y: 270
        width: 50
        height: 50
        radius: 5
        color: area_9.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_9
            anchors.fill: parent
            onClicked: cal.input_text += "9"
        }
        Text {
            text: "9"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_6
        x: 280
        y: 335
        width: 50
        height: 50
        radius: 5
        color: area_6.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_6
            anchors.fill: parent
            onClicked: cal.input_text += "6"
        }
        Text {
            text: "6"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_3
        x: 280
        y: 400
        width: 50
        height: 50
        radius: 5
        color: area_3.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_3
            anchors.fill: parent
            onClicked: cal.input_text += "3"
        }
        Text {
            text: "3"
            font.pointSize: 30
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }
    }

    Rectangle {
        id: key_res
        x: 280
        y: 465
        width: 50
        height: 50
        radius: 5
        color: area_res.pressed ? "#fff" : "transparent"
        Image {
            anchors.fill: parent
            source: "../images/frosty_button2.png"
        }
        Image {
            anchors.fill: parent
            source: cal.theme ? "../images/frosty_button_red.png" : "../images/frosty_button_blue.png"
        }
        MouseArea {
            id: area_res
            anchors.fill: parent
            onClicked: cal.calculate("", "result")
        }
        Text {
            anchors.fill: parent
            text: "="
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 30
        }
    }
}

QRC = qml
SRC = src
HDR = hdr

TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += \
    $$SRC/main.cpp

RESOURCES += \
    $$QRC/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    $$HDR/main.h

DISTFILES += \
    key.qml

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../math_lib/lib/release/ -lmath
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../math_lib/lib/debug/ -lmath
else:unix: LIBS += -L$$PWD/../math_lib/lib/ -lmath

INCLUDEPATH += $$PWD/../math_lib/lib/debug
DEPENDPATH += $$PWD/../math_lib/lib/debug

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/libmath.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/libmath.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/math.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/math.lib
else:unix: PRE_TARGETDEPS += $$PWD/../math_lib/lib/libmath.a


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../math_lib/lib/release/ -lmath
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../math_lib/lib/debug/ -lmath
else:unix: LIBS += -L$$PWD/../math_lib/lib/ -lmath

INCLUDEPATH += $$PWD/../math_lib/lib/release
DEPENDPATH += $$PWD/../math_lib/lib/release

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/libmath.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/libmath.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/release/math.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../math_lib/lib/debug/math.lib
else:unix: PRE_TARGETDEPS += $$PWD/../math_lib/lib/libmath.a

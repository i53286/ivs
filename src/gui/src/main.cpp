#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QColor>
#include <QDebug>
#include <QtQuick>

#include "../hdr/main.h"
#include "../math_lib/hdr/math_lib.h"

Cal::Cal(QObject *parent) :
    QObject(parent) {
}

double Cal::result(QString a, QString b, QString oper) {
    double res = a.toDouble();
    qDebug() << "RESULT " << res;
    double inp = b.toDouble();
    qDebug() << "INPUT " << inp;
    qDebug() << "OPER " << oper;

try {
    if (oper == "plus") {
        return Math_lib::Add(res, inp);
    }
    else if (oper == "minus") {
        return Math_lib::Sub(res, inp);
    }
    else if (oper == "slash") {
        return Math_lib::Div(res, inp);
    }
    else if (oper == "asterisk") {
        return Math_lib::Mul(res, inp);
    }
    else if (oper == "root") {
        return Math_lib::Root(res, inp);
    }
    else if (oper == "rootn") {
        return Math_lib::Root(res, inp);
    }
    else if (oper == "power") {
        return Math_lib::Power(res, inp);
    }
    else if (oper == "factorial") {
        return Math_lib::Factorial(res);
    }
    else if (oper == "percent") {
        return Math_lib::Percent(res, inp);
    }
}
    catch (...) {
        return std::numeric_limits<double>::quiet_NaN();
    }

    return res;
}

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    app.setFont(QFont("Lucida Handwriting"));
    app.setWindowIcon(QIcon("../images/ikona_frosty.ico"));

    qmlRegisterType<Cal>("Cal", 1, 0, "Cal");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

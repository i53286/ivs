#ifndef MAIN_H
#define MAIN_H
#include <QObject>
#include <QtQuick>
#include <QCoreApplication>
#include <QAbstractItemModel>

class Cal : public QObject {
    Q_OBJECT
public:
    explicit Cal(QObject *parent = 0);
    Q_INVOKABLE double result(QString a, QString b, QString oper);
signals:

public slots:
};

#endif // MAIN_H

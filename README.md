## Prostredi ##

* Windows 64bit
* Windows 32bit

## Autori ##
No StackOverflow, no degree

* xrezna04 Michal Režňák
* xrusno02 Anna Popovská
* xsalus00 Ondřej Salus
* xrysav25 Michaela Ryšavá

## Licence ##

Tento program je poskytovan s licenci GNU General Public Licence, GPL v3.0

## Instalace zavislosti pro vyvoj ##
* Vsechny potrebne informace jsou v souboru **Instalace.txt**

## Pokyny pro praci s gitem ##
Vsechny prikazy jde provadet v GUI

#### Pred zacatkem prace ####
* aktualizovat repozitar: 'git pull'
* pro zobrazeni stavu repozitare: 'git status'
* vytvorit novou vetev (branch): 'git branch origin (jmeno vetve)'
* prepnout se do nove vetve: 'git checkout (jmeno vetve)'

#### Pri praci ####
* Kazdy soubor ktery byl upraveny se musi pridat: 'git add (jmeno souboru [slozky], lze zobrazit pomoci 'git status')'

#### Po praci ####
* Potvrdit vsechny zmeny: 'git commit'
* **Pokud funkce je hotova:**
    * Prepnout se do hlavni vetve: 'git checkout master'
    * Spojit zmeny ktere jste provedli ve sve vetvy: 'git merge (jmeno vetve)'
    * Nahrat zmeny na vzdaleny repozitar: 'git push origin master'
* **Jinak:**
    * Nahrat zmeny na vzdaleny repozitar: 'git push origin (jmeno vetve)'

## Spravny zpusob commitu ##
Prvni je kapitalkami napsane klicove slovo, hned za nim je kratky popisek oddeleny mezerou.
Potom je vynechany radek (2x enter) a nasleduje dlouhy popisek pouze pokud je to potreba.
Priklad:     
   
    ADD kratky popisek  
  
    Dlouhy popisek pouze pokud je potreba.  

### Klicova slova ###
* INIT -- prvni commit pro inicializaci repozitare
* ADD -- pridani souboru
* CHANGE -- zmena v souboru, nebo implementaci
* REMOVE -- odstraneni souboru

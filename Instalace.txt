### GIT ###
* nainstalovat git 
	https://git-scm.com/download/win
	* next -> next -> 
	* 'Use Git from GIt Bash only' -> next 
	* 'Use the openSSL library -> next 
	* 'Checkout as-is, commit as-is -> next
	* 'Use MinTTY' -> next 
	* 'Enable (vsechno zaskrtnute jako ano)' -> Install

### QT ###
(kroky co me fungovali, nezarucuji ze budou taky):

* odinstalovat cele qt, mingw, make, cmake

* pokud byl nainstalovany Visual Studio 2017, tak odinstalovat Microsoft Visual C++ 2017 Redistributable

* nainstalovat Microsoft visual build tools
 
	http://landinghub.visualstudio.com/visual-cpp-build-tools 
    
	** pri instalaci zaskrtnout windows 8.1 a windows 10, jinak nic

* nainstalovat Qt
	https://www.qt.io/download-open-source/?hsCtaTracking=f977210e-de67-475f-a32b-65cec207fd03%7Cd62710cd-e1db-46aa-8d4d-	2f1c1ffdacea 
    
	** pri instalaci zaskrtnout v kategorii Qt 5.8 (nezaskrtavat celou): 
        
		* msvc2015 64-bit
        
		* Vse co zacina na Qt ve skupine Qt 5.8 nechat
        
		* ostatni vsechno odznacit, nechat pouze Qt Creator.

* Podivat se do Tools -> Options... na nastaveni Kits a Compilers **(obrazky)**


* Zkusit zalozit projekt Qt Widgets a jenom spustit pomoci Ctrl+r, melo by se otevrit prazdne okno


### Repozitar ###
* v adresari ve kterem chcete vytvorit repozitar -> pravym -> Git Bash Here
* zadat: git clone --recursive git@bitbucket.org:i53286/ivs_test.git

### GoogleTesty ###
* otevrit soubor 'ivs\src\googletest\googletest.pro'
* otevre se Qt creator a v nem okno s nastavenim (Projects), kliknout na 'Configure Project'
* pomoci Ctrl+b se provede build, pomoci Ctrl+r se program spusti
* google testy se pisou do souboru 'calc_lib_tests.h' (je tam napsany jeden ukazkove)
* bude mozna potreba pri psani testu smazat vzdy adresar 'build-*', ktery se vytvori vedle slozky 'googletest' a 'math_lib'

### Math_lib ###
* otevrit soubor 'ivs\src\math_lib\math_lib.pro'
* otevre se Qt creator a v nem okno s nastavenim (Projects), kliknout na 'Configure Project'
* pomoci Ctrl+b se provede build
* pro testovani v Google testech se musi cela knihovna i s debug informacemy (ivs\src\build-math_lib-*\debug\) skopirovat do slozky 'ivs\src\math_lib\lib'
